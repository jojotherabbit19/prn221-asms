﻿namespace Domain.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public string ProductCode { get; set; }
        public decimal Price { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
