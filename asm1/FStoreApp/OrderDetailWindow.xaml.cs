﻿using Accessibility;
using Applications.IService;
using Applications.ViewModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Linq;

namespace FStoreApp
{
    /// <summary>
    /// Interaction logic for OrderDetailWindow.xaml
    /// </summary>
    public partial class OrderDetailWindow : Window
    {
        private readonly IUserService _userService;
        private readonly IProductService _productService;
        private readonly IOrderService _orderService;
        private readonly IOrderDetailService _orderDetailService;
        private readonly int _orderId;
        private readonly decimal _total;
        public OrderDetailWindow(IUserService userService,
            IProductService productService,
            IOrderService orderService,
            IOrderDetailService orderDetailService,
            int orderId,
            decimal total)
        {
            InitializeComponent();
            _userService = userService;
            _productService = productService;
            _orderService = orderService;
            _orderDetailService = orderDetailService;
            _orderId = orderId;
            _total = total;
            Load();
            txtOrderId.IsReadOnly = true;
            txtProduct.IsReadOnly = true;
            txtUnitPrice.IsReadOnly = true;
            txtTotalPrice.IsReadOnly = true;
            txtOrderId.Text = orderId.ToString();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }
        private async void Load()
        {
            var result = await _productService.GetAllProducts();
            LoadOrderDetail();
            if (result is null)
            {
                MessageBox.Show("Product is empty");
            }
            else
            {
                productTable.ItemsSource = result;
            }
        }
        private async void LoadOrderDetail()
        {
            var result = await _orderDetailService.GetOrderDetail(_orderId);
            decimal total = 0;
            if (result is null)
            {
                MessageBox.Show("Pls add product to your order");
                total = _total;
                return;
            }
            else
            {
                orderDetailTable.ItemsSource = result;
                foreach (var item in result)
                {
                    total += item.Total;
                }
                txtTotalPrice.Text = total.ToString();
            }
        }

        private void productTable_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var item = productTable.SelectedItem as ProductViewModel;
            if (item is null)
            {
                Clear();
            }
            else
            {
                Clear();
                txtProduct.Text = item.Id.ToString();
                txtUnitPrice.Text = item.Price.ToString();
            }
        }

        private async void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var orderDetail = new OrderDetailViewModel
            {
                OrderId = _orderId,
                ProductId = int.Parse(txtProduct.Text),
                Quantity = int.Parse(txtQuantity.Text),
                Total = decimal.Parse(txtQuantity.Text) * decimal.Parse(txtUnitPrice.Text)
            };
            var result = await _orderDetailService.CreateOrderDetail(orderDetail);
            if (result)
            {
                MessageBox.Show("Add Success");
                Clear();
                Load();
            }
            else
            {
                MessageBox.Show("Add Fail");
            }
        }

        private async void orderDetailTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = orderDetailTable.SelectedItem as OrderDetailViewModel;
            if (item is null)
            {
                Clear();
            }
            else
            {
                Clear();
                var product = await _productService.GetProductById(item.ProductId);
                txtProduct.Text = item.ProductId.ToString();
                txtUnitPrice.Text = product.Price.ToString();
                txtQuantity.Text = item.Quantity.ToString();
                txtTotalUnit.Text = item.Total.ToString();
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }
        private void Clear()
        {
            txtProduct.Text = "";
            txtUnitPrice.Text = "";
            txtQuantity.Text = "";
            txtTotalUnit.Text = "";
            txtTotalPrice.Text = "";
        }

        private async void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            var detail = new OrderDetailViewModel
            {
                OrderId = int.Parse(txtOrderId.Text),
                ProductId = int.Parse(txtProduct.Text),
                Quantity = int.Parse(txtQuantity.Text),
                Total = decimal.Parse(txtQuantity.Text) * decimal.Parse(txtUnitPrice.Text)
            };
            var result = await _orderDetailService.UpdateOrderDetail(detail);
            if (result)
            {
                MessageBox.Show("Updated");
                Clear();
                Load();
            }
            else
            {
                MessageBox.Show("Fail");
            }
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            var result = await _orderDetailService.DeleteOrderDetail(int.Parse(txtOrderId.Text), int.Parse(txtProduct.Text));
            if (result)
            {
                MessageBox.Show("Deleted");
                Clear();
                Load();
            }
            else
            {
                MessageBox.Show("Fail");
            }
        }

        private async void btnSave_Click(object sender, RoutedEventArgs e)
        {
            var order = await _orderService.GetOrderById(int.Parse(txtOrderId.Text));
            order.Total = decimal.Parse(txtTotalPrice.Text);
            var result = await _orderService.UpdateOrder(order);
            if (result)
            {
                MessageBox.Show("Order Success");
                this.Close();
                // sửa sau khi đóng thì refresh lại main
            }
            else
            {
                MessageBox.Show("Error");
                return;
            }
        }
    }
}
