﻿using Applications.IService;
using System.Windows;

namespace FStoreApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IUserService _userService;
        private readonly IProductService _productService;
        private readonly IOrderService _orderService;
        private readonly IOrderDetailService _orderDetailService;
        public MainWindow(IUserService userService,
            IProductService productService,
            IOrderService orderService,
            IOrderDetailService orderDetailService)
        {
            InitializeComponent();
            _userService = userService;
            _productService = productService;
            _orderService = orderService;
            _orderDetailService = orderDetailService;
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }

        private void btnUserPage_Clicked(object sender, RoutedEventArgs e)
        {
            Main.Content = new User(_userService);
        }

        private void ProductPage_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new Product(_productService);
        }

        private void OrderPage_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new Order(_orderService, _orderDetailService, _userService, _productService);
        }
    }
}
