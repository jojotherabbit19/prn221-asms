﻿using Applications.IService;
using System.Windows;

namespace FStoreApp
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        private readonly IUserService _userService;
        private readonly IProductService _productService;
        private readonly IOrderService _orderService;
        private readonly IOrderDetailService _orderDetailService;
        public Login(IUserService userService,
            IProductService productService,
            IOrderService orderService,
            IOrderDetailService orderDetailService)
        {
            InitializeComponent();
            _userService = userService;
            _productService = productService;
            _orderService = orderService;
            _orderDetailService = orderDetailService;
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            if (txtUserName.Text == "admin" && txtPassword.Text == "1")
            {
                MainWindow main = new MainWindow(_userService, _productService, _orderService, _orderDetailService);
                main.Show();
                this.Close();
            }
        }
    }
}
