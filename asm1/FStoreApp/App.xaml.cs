﻿using Applications.IService;
using Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Windows;

namespace FStoreApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly IHost _host;
        public App()
        {
            _host = Host.CreateDefaultBuilder().ConfigureServices((service) =>
            {
                DependencyInjection.MyService(service);
                service.AddScoped<Login>();
            }).Build();
        }
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            _host.StartAsync();
            // Resolve and create an instance of the Login class with dependencies
            Login login = _host.Services.GetRequiredService<Login>();
            login.Show();
        }
    }
}
