﻿using Applications.IService;
using Applications.ViewModel;
using System.Windows;
using System.Windows.Controls;

namespace FStoreApp
{
    /// <summary>
    /// Interaction logic for User.xaml
    /// </summary>
    public partial class User : Page
    {
        private readonly IUserService _userService;
        public User(IUserService userService)
        {
            InitializeComponent();
            _userService = userService;
            LoadAsync();
        }

        private async void LoadAsync()
        {
            var users = await _userService.GetAllUsers();
            if (users is null)
            {
                MessageBox.Show("User List is empty");
            }
            dataGrid.ItemsSource = users;
        }

        private async void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var user = new UserViewModel
            {
                UserName = txtUserName.Text,
                PasswordHash = txtPassword.Text,
                Phone = txtPhone.Text,
            };
            var result = await _userService.CreateUser(user);
            if (result)
            {
                MessageBox.Show("Add Success");
                txtUserName.Text = "";
                txtPassword.Text = "";
                txtPhone.Text = "";
                LoadAsync();
            }
            else
            {
                MessageBox.Show("Add Fail");
            }
        }

        private async void btnShow_Click(object sender, RoutedEventArgs e)
        {
            var users = await _userService.GetAllUsers();
            if (users is null)
            {
                MessageBox.Show("User List is empty");
            }
            dataGrid.ItemsSource = users;
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = dataGrid.SelectedItem as UserViewModel;
            if (item != null)
            {
                txtId.Content = item.Id;
                txtUserName.Text = item.UserName;
                txtPassword.Text = item.PasswordHash;
                txtPhone.Text = item.Phone;
            }
        }

        private async void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            var userobj = await _userService.ExistUserName(txtUserName.Text);
            if (userobj is null)
            {
                MessageBox.Show("Not found user");
                return;
            }

            userobj.PasswordHash = txtPassword.Text;
            userobj.Phone = txtPhone.Text;

            var result = await _userService.UpdateUser(userobj);
            if (result)
            {
                MessageBox.Show("Update Success");
                txtUserName.Text = "";
                txtPassword.Text = "";
                txtPhone.Text = "";
                LoadAsync();
            }
            else
            {
                MessageBox.Show("Update Fail");
            }
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            var result = await _userService.DeleteUser(int.Parse(txtId.Content.ToString()));
            if (result)
            {
                MessageBox.Show("Deleted");
                txtUserName.Text = "";
                txtPassword.Text = "";
                txtPhone.Text = "";
                LoadAsync();
            }
            else
            {
                MessageBox.Show("Delete fail");
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtId.Content = null;
            txtUserName.Text = null;
            txtPassword.Text = null;
            txtPhone.Text = null;
        }
    }
}
