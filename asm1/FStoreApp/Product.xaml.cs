﻿using Applications.IService;
using Applications.Service;
using Applications.ViewModel;
using System.Windows;
using System.Windows.Controls;

namespace FStoreApp
{
    /// <summary>
    /// Interaction logic for Product.xaml
    /// </summary>
    public partial class Product : Page
    {
        private readonly IProductService _productService;
        public Product(IProductService productService)
        {
            InitializeComponent();
            _productService = productService;
            LoadAsync();
        }

        private async void LoadAsync()
        {
            var products = await _productService.GetAllProducts();
            if (products is null)
            {
                MessageBox.Show("User List is empty");
            }
            dataGrid.ItemsSource = products;
        }
        private async void btnAdd_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            var product = new ProductViewModel
            {
                Name = txtName.Text,
                Price = decimal.Parse(txtPrice.Text.ToString()),
                ProductCode = txtProductCode.Text
            };
            var result = await _productService.CreateProduct(product);
            if (result)
            {
                MessageBox.Show("Add Success");
                txtId.Text = "";
                txtName.Text = "";
                txtPrice.Text = "";
                txtProductCode.Text = "";
                LoadAsync();
            }
            else
            {
                MessageBox.Show("Add Fail");
            }
        }

        private async void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            var product = new ProductViewModel
            {
                Id = int.Parse(txtId.Text),
                Name = txtName.Text,
                Price = decimal.Parse(txtPrice.Text),
                ProductCode = txtProductCode.Text
            };
            var result = await _productService.UpdateProduct(product);
            if (result)
            {
                MessageBox.Show("Updated");
                txtId.Text = "";
                txtName.Text = "";
                txtPrice.Text = "";
                txtProductCode.Text = "";
                LoadAsync();
            }
            else
            {
                MessageBox.Show("Fail");
            }
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = dataGrid.SelectedItem as ProductViewModel;
            if (item != null)
            {
                txtId.Text = item.Id.ToString();
                txtName.Text = item.Name;
                txtPrice.Text = item.Price.ToString();
                txtProductCode.Text = item.ProductCode;
            }
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            var result = await _productService.DeleteProduct(int.Parse(txtId.Text));
            if (result)
            {
                MessageBox.Show("Deleted");
                txtId.Text = "";
                txtName.Text = "";
                txtPrice.Text = "";
                txtProductCode.Text = "";
                LoadAsync();
            }
            else
            {
                MessageBox.Show("Delete fail");
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtId.Text = "";
            txtName.Text = "";
            txtPrice.Text = "";
            txtProductCode.Text = "";
        }
    }
}
