﻿using Applications.IService;
using Applications.ViewModel;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace FStoreApp
{
    /// <summary>
    /// Interaction logic for Order.xaml
    /// </summary>
    public partial class Order : Page
    {
        private readonly IOrderService _orderService;
        private readonly IOrderDetailService _orderDetailService;
        private readonly IUserService _userService;
        private readonly IProductService _productService;
        public Order(IOrderService orderService, IOrderDetailService orderDetailService, IUserService userService, IProductService productService)
        {
            InitializeComponent();
            _orderService = orderService;
            _orderDetailService = orderDetailService;
            _userService = userService;
            _productService = productService;
            LoadAsync();
        }

        private async void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = dataGrid.SelectedItem as OrderViewModel;
            if (item is null)
            {
                txtOrderId.IsReadOnly = true;
                txtUser.IsReadOnly = true;
                txtDate.IsReadOnly = true;
                txtTotal.IsReadOnly = true;
                txtOrderId.Text = "";
                txtUser.Text = "";
                txtTotal.Text = "";
                txtDate.Text = "";
            }
            else
            {
                var user = await _userService.GetByIdAsync(item.UserId);
                txtOrderId.IsReadOnly = true;
                txtUser.IsReadOnly = true;
                txtDate.IsReadOnly = true;
                txtTotal.IsReadOnly = true;
                txtOrderId.Text = item.Id.ToString();
                txtUser.Text = user.UserName;
                txtTotal.Text = item.Total.ToString();
                txtDate.Text = item.OrderDate.ToString();
            }
        }
        private async void LoadAsync()
        {
            await UserComboBox();
            var orders = await _orderService.GetAllOrder();
            if (orders is null)
            {
                MessageBox.Show("No order yet");
            }
            dataGrid.ItemsSource = orders;
        }

        private async Task UserComboBox()
        {
            var user = await _userService.GetAllUsers();
            if (user is null)
            {
                userComboBox.ItemsSource = "";
            }
            else
            {
                userComboBox.ItemsSource = user.Select(x => x.UserName);
                userComboBox.SelectedIndex = 0;
            }
        }

        private async void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            var result = await _orderService.DeleteOrder(int.Parse(txtOrderId.Text));
            if (result)
            {
                MessageBox.Show("Deleted");
                txtOrderId.Text = "";
                txtUser.Text = "";
                txtDate.Text = "";
                txtTotal.Text = "";
                LoadAsync();
            }
            else
            {
                MessageBox.Show("Delete fail");
            }
        }

        private async void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var selectedUser = userComboBox.SelectedItem.ToString();
            var user = await _userService.ExistUserName(selectedUser);
            var order = new OrderViewModel
            {
                UserId = user.Id,
                OrderDate = DateTime.UtcNow,
                Total = 0
            };
            var result = await _orderService.CreateOrder(order);
            if (result)
            {
                MessageBox.Show("Add Success");
                txtOrderId.Text = "";
                txtUser.Text = "";
                txtDate.Text = "";
                txtTotal.Text = "";
                LoadAsync();
            }
            else
            {
                MessageBox.Show("Add Fail");
            }
        }

        private void btnAddProduct_Click(object sender, RoutedEventArgs e)
        {
            if(string.IsNullOrEmpty(txtOrderId.Text))
            {
                MessageBox.Show("Select OrderId pls");
                return;
            }
            OrderDetailWindow detailWindow = new OrderDetailWindow(_userService, _productService, _orderService, _orderDetailService, int.Parse(txtOrderId.Text), decimal.Parse(txtTotal.Text));
            detailWindow.Show();
        }
    }
}
