﻿using Applications;
using Applications.IService;
using Applications.Service;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
namespace Infrastructure
{
    public static class DependencyInjection
    {
        private static string conn = "Server=127.0.0.1;Port=5432;Database=Asm01;User Id=postgres;Password=123;";
        public static IServiceCollection MyService(IServiceCollection service)
        {
            service.AddDbContext<AppDbContext>(op => op.UseNpgsql(conn));
            service.AddScoped<IUnitOfWork, UnitOfWork>();
            service.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            service.AddScoped<IUserService, UserService>();
            service.AddScoped<IProductService, ProductService>();
            service.AddScoped<IOrderService, OrderService>();
            service.AddScoped<IOrderDetailService, OrderDetailService>();
            return service;
        }
    }
}
