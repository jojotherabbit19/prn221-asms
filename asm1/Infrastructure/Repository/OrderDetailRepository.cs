﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repository
{
    public class OrderDetailRepository : GenericRepository<OrderDetail>, IOrderDetailRepository
    {
        public OrderDetailRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<OrderDetail?> GetOrderDetailsById(int OrderId, int ProductId)
        {
            return await dbSet.FirstOrDefaultAsync(x=>x.OrderId == OrderId && x.ProductId == ProductId);
        }

        public async Task<List<OrderDetail>?> GetOrderDetailsByOrderId(int id)
        {
            return await dbSet.Where(x => x.OrderId == id).ToListAsync();
        }
    }
}
