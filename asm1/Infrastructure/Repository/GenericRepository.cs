﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected readonly DbSet<T> dbSet;
        public GenericRepository(AppDbContext context)
        {
            dbSet = context.Set<T>();
        }
        public async Task CreateAsync(T entity)
        {
            await dbSet.AddAsync(entity);
        }

        public void Delete(T entity)
        {
            dbSet.Remove(entity);
        }

        public async Task<List<T>?> GetAll()
        {
            return await dbSet.ToListAsync();
        }

        public Task<T?> GetById(int id)
        {
            return dbSet.FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(T entity)
        {
            dbSet.Update(entity);
        }
    }
}
