﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repository
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<User?> ExistUserName(string username) => await dbSet.FirstOrDefaultAsync(x => x.UserName == username);

        public Task<User?> Login(string username, string password)
        {
            return dbSet.FirstOrDefaultAsync(x => x.UserName == username && x.PasswordHash == password);
        }
    }
}
