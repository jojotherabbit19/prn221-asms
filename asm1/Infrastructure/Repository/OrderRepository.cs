﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repository
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<List<Order>?> GetOrderByUser(int userId)
            => await dbSet.Where(x => x.UserId == userId)
                          .Include(x => x.OrderDetails)
                          .ToListAsync();

        public async Task<Order?> GetOrderDetail(int userId, int orderId)
        {
            return await dbSet.Include(x => x.OrderDetails)
                              .ThenInclude(x => x.Product)
                              .FirstOrDefaultAsync(x => x.UserId == userId && x.Id == orderId);
        }
    }
}
