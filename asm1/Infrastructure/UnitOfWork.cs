﻿using Applications;
using Infrastructure.Repository;

namespace Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _dbContext;
        public UnitOfWork(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IOrderDetailRepository OrderDetailRepository => new OrderDetailRepository(_dbContext);

        public IOrderRepository OrderRepository => new OrderRepository(_dbContext);

        public IProductRepository ProductRepository => new ProductRepository(_dbContext);

        public IUserRepository UserRepository => new UserRepository(_dbContext);

        public Task<int> SaveChangeAsync()
        {
            return _dbContext.SaveChangesAsync();
        }
    }
}
