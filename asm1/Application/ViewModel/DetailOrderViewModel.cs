﻿namespace Applications.ViewModel
{
    public class DetailOrderViewModel
    {
        public int UserId { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal Total { get; set; }
        public List<OrderDetailViewModel> Details { get; set; }
    }
}
