﻿namespace Applications.ViewModel
{
    public class UpdateUserViewModel
    {
        public string PasswordHash { get; set; }
        public string Phone { get; set; }
    }
}
