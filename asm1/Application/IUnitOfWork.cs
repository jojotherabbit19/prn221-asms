﻿namespace Applications
{
    public interface IUnitOfWork
    {
        public IOrderDetailRepository OrderDetailRepository { get; }
        public IOrderRepository OrderRepository { get; }
        public IProductRepository ProductRepository { get; }
        public IUserRepository UserRepository { get; }
        Task<int> SaveChangeAsync();
    }
}
