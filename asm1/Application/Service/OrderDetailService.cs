﻿using Applications.IService;
using Applications.ViewModel;
using AutoMapper;
using Domain.Entities;

namespace Applications.Service
{
    public class OrderDetailService : IOrderDetailService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public OrderDetailService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<bool> CreateOrderDetail(OrderDetailViewModel orderDetail)
        {
            var obj = _mapper.Map<OrderDetail>(orderDetail);
            await _unitOfWork.OrderDetailRepository.CreateAsync(obj);
            var result = await _unitOfWork.SaveChangeAsync();
            if (result > 0)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> DeleteOrderDetail(int OrderId, int ProductId)
        {
            var obj = await _unitOfWork.OrderDetailRepository.GetOrderDetailsById(OrderId, ProductId);
            if (obj is null)
            {
                throw new ArgumentException("Not found");
            }
            _unitOfWork.OrderDetailRepository.Delete(obj);
            var result = await _unitOfWork.SaveChangeAsync();
            if (result > 0)
            {
                return true;
            }

            return false;
        }

        public async Task<OrderViewModel?> GetOrder(int id)
        {
            return _mapper.Map<OrderViewModel>(await _unitOfWork.OrderRepository.GetById(id));
        }

        public async Task<List<OrderDetailViewModel>?> GetOrderDetail(int id)
        {
            var result = await _unitOfWork.OrderDetailRepository.GetOrderDetailsByOrderId(id);
            return _mapper.Map<List<OrderDetailViewModel>>(result);
        }

        public async Task<bool> UpdateOrderDetail(OrderDetailViewModel orderDetail)
        {
            var obj = _mapper.Map<OrderDetail>(await _unitOfWork.OrderDetailRepository.GetOrderDetailsById(orderDetail.OrderId, orderDetail.ProductId));
            if (obj is null)
            {
                return false;
            }
            else
            {
                obj.Quantity = orderDetail.Quantity;
                obj.Total = orderDetail.Total;
            }
            var result = await _unitOfWork.SaveChangeAsync();
            if (result > 0)
            {
                return true;
            }

            return false;
        }
    }
}
