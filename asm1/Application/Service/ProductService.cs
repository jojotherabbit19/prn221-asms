﻿using Applications.IService;
using Applications.ViewModel;
using AutoMapper;
using Domain.Entities;

namespace Applications.Service
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<bool> CreateProduct(ProductViewModel product)
        {
            var obj = _mapper.Map<Product>(product);
            await _unitOfWork.ProductRepository.CreateAsync(obj);
            var result = await _unitOfWork.SaveChangeAsync();
            if (result > 0)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> DeleteProduct(int id)
        {
            var obj = await _unitOfWork.ProductRepository.GetById(id);
            if (obj is null)
            {
                throw new ArgumentException("Not found");
            }
            _unitOfWork.ProductRepository.Delete(obj);
            var result = await _unitOfWork.SaveChangeAsync();
            if (result > 0)
            {
                return true;
            }

            return false;
        }

        public async Task<List<ProductViewModel>> GetAllProducts()
        {
            return _mapper.Map<List<ProductViewModel>>(await _unitOfWork.ProductRepository.GetAll());
        }

        public async Task<ProductViewModel> GetProductById(int id)
        {
            return _mapper.Map<ProductViewModel>(await _unitOfWork.ProductRepository.GetById(id));
        }

        public async Task<bool> UpdateProduct(ProductViewModel product)
        {
            var obj = await _unitOfWork.ProductRepository.GetById(product.Id);
            if (obj is null)
            {
                throw new Exception($"{product.Name} is null");
            }

            obj.Name = product.Name;
            obj.Price = product.Price;
            obj.ProductCode = product.ProductCode;

            _unitOfWork.ProductRepository.Update(obj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return true;
            }

            return false;
        }
    }
}
