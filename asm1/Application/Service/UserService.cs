﻿using Applications.IService;
using Applications.ViewModel;
using AutoMapper;
using Domain.Entities;

namespace Applications.Service
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<bool> CreateUser(UserViewModel user)
        {
            var userObj = _mapper.Map<User>(user);
            await _unitOfWork.UserRepository.CreateAsync(userObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> DeleteUser(int id)
        {
            var user = await _unitOfWork.UserRepository.GetById(id);
            if (user is null)
            {
                throw new Exception("Not found user");
            }
            _unitOfWork.UserRepository.Delete(user);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return true;
            }

            return false;
        }

        public async Task<UserViewModel?> ExistUserName(string username)
        {
            return _mapper.Map<UserViewModel?>(await _unitOfWork.UserRepository.ExistUserName(username));
        }

        public async Task<List<UserViewModel>> GetAllUsers()
            => _mapper.Map<List<UserViewModel>>(await _unitOfWork.UserRepository.GetAll());

        public async Task<UserViewModel> GetByIdAsync(int id)
        {
            return _mapper.Map<UserViewModel>(await _unitOfWork.UserRepository.GetById(id));
        }

        public async Task<bool> Login(string username, string password)
        {
            var user = await _unitOfWork.UserRepository.Login(username, password);
            if (user is null)
            {
                throw new Exception("Not found user");
            }

            return true;
        }

        public async Task<bool> UpdateUser(UserViewModel user)
        {
            var obj = await _unitOfWork.UserRepository.ExistUserName(user.UserName);
            if (obj is null)
            {
                throw new Exception($"{user.UserName} is null");
            }
            obj.PasswordHash = user.PasswordHash;
            obj.Phone = user.Phone;
            _unitOfWork.UserRepository.Update(obj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return true;
            }

            return false;
        }
    }
}
