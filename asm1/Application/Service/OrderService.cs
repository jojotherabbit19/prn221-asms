﻿using Applications.IService;
using Applications.ViewModel;
using AutoMapper;
using Domain.Entities;

namespace Applications.Service
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<bool> CreateOrder(OrderViewModel order)
        {
            var obj = _mapper.Map<Order>(order);
            await _unitOfWork.OrderRepository.CreateAsync(obj);
            var result = await _unitOfWork.SaveChangeAsync();
            if (result > 0)
            {
                return true;
            }

            return false;
        }

        public async Task<bool> DeleteOrder(int id)
        {
            var obj = await _unitOfWork.OrderRepository.GetById(id);
            if (obj is null)
            {
                throw new ArgumentException("Not found");
            }
            _unitOfWork.OrderRepository.Delete(obj);
            var result = await _unitOfWork.SaveChangeAsync();
            if (result > 0)
            {
                return true;
            }

            return false;
        }

        public async Task<List<OrderViewModel>> GetAllOrder()
        {
            return _mapper.Map<List<OrderViewModel>>(await _unitOfWork.OrderRepository.GetAll());
        }

        public Task<DetailOrderViewModel> GetDetailOrder(int userId, int orderId)
        {
            throw new NotImplementedException();
        }

        public async Task<OrderViewModel> GetOrderById(int id)
        {
            return _mapper.Map<OrderViewModel>(await _unitOfWork.OrderRepository.GetById(id));
        }

        public Task<OrderViewModel> GetOrderByUser(int userId)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpdateOrder(OrderViewModel order)
        {
            var obj = await _unitOfWork.OrderRepository.GetById(order.Id);
            if (obj is null)
            {
                throw new Exception($"{order.Id} is null");
            }

            obj.Total = order.Total;
            obj.OrderDate = DateTime.UtcNow;

            _unitOfWork.OrderRepository.Update(obj);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                return true;
            }

            return false;
        }
    }
}
