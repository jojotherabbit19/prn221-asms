﻿using Domain.Entities;

public interface IProductRepository : IGenericRepository<Product>
{
}