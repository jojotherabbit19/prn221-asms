﻿public interface IGenericRepository<T>
{
    Task CreateAsync(T entity);
    void Update(T entity);
    void Delete(T entity);
    Task<T?> GetById(int id);
    Task<List<T>?> GetAll();
}