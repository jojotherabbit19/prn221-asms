﻿using Domain.Entities;

public interface IUserRepository : IGenericRepository<User>
{
    Task<User?> Login(string username, string password);
    Task<User?> ExistUserName(string username);
}