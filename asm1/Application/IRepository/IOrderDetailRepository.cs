﻿using Domain.Entities;

public interface IOrderDetailRepository : IGenericRepository<OrderDetail>
{
    Task<List<OrderDetail>?> GetOrderDetailsByOrderId(int id);
    Task<OrderDetail?> GetOrderDetailsById(int OrderId, int ProductId);
}