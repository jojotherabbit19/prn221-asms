﻿using Domain.Entities;

public interface IOrderRepository : IGenericRepository<Order>
{
    Task<Order?> GetOrderDetail(int userId, int orderId);
    Task<List<Order>?> GetOrderByUser(int userId);
}