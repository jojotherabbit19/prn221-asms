﻿using Applications.ViewModel;

namespace Applications.IService
{
    public interface IOrderService
    {
        Task<bool> CreateOrder(OrderViewModel order);
        Task<bool> UpdateOrder(OrderViewModel order);
        Task<bool> DeleteOrder(int id);
        Task<List<OrderViewModel>> GetAllOrder();
        Task<DetailOrderViewModel> GetDetailOrder(int userId, int orderId);
        Task<OrderViewModel> GetOrderByUser(int userId);
        Task<OrderViewModel> GetOrderById(int id);

    }
}
