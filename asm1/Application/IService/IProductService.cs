﻿using Applications.ViewModel;
using Domain.Entities;

namespace Applications.IService
{
    public interface IProductService
    {
        Task<bool> CreateProduct(ProductViewModel product);
        Task<bool> UpdateProduct(ProductViewModel product);
        Task<bool> DeleteProduct(int id);
        Task<List<ProductViewModel>> GetAllProducts();
        Task<ProductViewModel> GetProductById(int id);
    }
}
