﻿using Applications.ViewModel;
using Domain.Entities;

namespace Applications.IService
{
    public interface IOrderDetailService
    {
        Task<bool> CreateOrderDetail(OrderDetailViewModel orderDetail);
        Task<bool> UpdateOrderDetail(OrderDetailViewModel orderDetail);
        Task<bool> DeleteOrderDetail(int OrderId, int ProductId);
        Task<List<OrderDetailViewModel>?> GetOrderDetail(int id);
    }
}
