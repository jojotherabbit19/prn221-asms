﻿using Applications.ViewModel;

namespace Applications.IService
{
    public interface IUserService
    {
        Task<UserViewModel> GetByIdAsync(int id);
        Task<bool> Login(string username, string password);
        Task<bool> CreateUser(UserViewModel user);
        Task<bool> UpdateUser(UserViewModel user);
        Task<UserViewModel?> ExistUserName(string username);
        Task<bool> DeleteUser(int id);
        Task<List<UserViewModel>> GetAllUsers();
    }
}
