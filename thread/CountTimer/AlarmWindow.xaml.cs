﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace CountTimer
{
    public partial class AlarmWindow : Window
    {
        public bool IsAlarmSet { get; private set; }
        public DateTime AlarmTime { get; private set; }

        public AlarmWindow()
        {
            InitializeComponent();
        }

        private void SetAlarmButton_Click(object sender, RoutedEventArgs e)
        {
            if (int.TryParse(HourInput.Text, out int hour) &&
                int.TryParse(MinuteInput.Text, out int minute) &&
                AmPmComboBox.SelectedItem is ComboBoxItem amPmItem)
            {
                int alarmHour = hour % 12; // Convert to 12-hour format
                int alarmMinute = minute;
                bool isAM = amPmItem.Content.ToString() == "AM";

                // Calculate the next alarm time
                DateTime now = DateTime.Now;
                DateTime nextAlarm = new DateTime(now.Year, now.Month, now.Day, alarmHour, alarmMinute, 0);
                if (!isAM && alarmHour != 12)
                {
                    nextAlarm = nextAlarm.AddHours(12);
                }

                if (nextAlarm <= now)
                {
                    // If the next alarm time has already passed today, set it for tomorrow
                    nextAlarm = nextAlarm.AddDays(1);
                }

                AlarmTime = nextAlarm;
                IsAlarmSet = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Invalid input for alarm time.");
            }
        }
    }


}
