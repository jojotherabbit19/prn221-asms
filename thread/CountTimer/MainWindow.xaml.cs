﻿using System;
using System.Threading;
using System.Windows;

namespace CountTimer
{
    public partial class MainWindow : Window
    {
        private System.Windows.Threading.DispatcherTimer timer;
        private Thread countdownThread;
        private TimeSpan remainingTime;
        private TimeSpan originalTime;
        private volatile bool isCountdownStopped = false;
        private volatile bool isCountdownPaused = false;
        private bool isAlarmSet = false;
        private DateTime alarmTime;
        private object lockObject = new object();

        public MainWindow()
        {
            InitializeComponent();

            timer = new System.Windows.Threading.DispatcherTimer();
            timer.Tick += new EventHandler(UpdateTimer);
            timer.Interval = TimeSpan.FromSeconds(1);
        }

        private void UpdateTimer(object sender, EventArgs e)
        {
            // Update the countdown label with the remaining time
            CountdownLabel.Content = remainingTime.ToString(@"hh\:mm\:ss");

            // Check if it's time for the alarm
            if (isAlarmSet && DateTime.Now >= alarmTime)
            {
                ShowAlarmMessage();
            }
        }

        private void ShowAlarmMessage()
        {
            isAlarmSet = false;
            timer.Stop();
            CountdownLabel.Content = "Wake Up";
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            // Disable the start button and enable the pause/resume, reset, and set alarm buttons
            StartButton.IsEnabled = false;
            PauseResumeButton.IsEnabled = true;
            ResetButton.IsEnabled = true;
            SetAlarmButton.IsEnabled = true;

            // Parse the user input for countdown time
            if (TimeSpan.TryParse("00:" + TimeInput.Text, out TimeSpan inputTime))
            {
                // Save the original input time
                originalTime = inputTime;

                // Start the countdown with the input time
                remainingTime = inputTime;
                isCountdownStopped = false;
                isCountdownPaused = false;
                countdownThread = new Thread(CountdownThreadFunction);
                countdownThread.Start();
            }
            else
            {
                MessageBox.Show("Invalid input time format. Please use the format 'mm:ss'.");
                StartButton.IsEnabled = true;
                PauseResumeButton.IsEnabled = false;
                ResetButton.IsEnabled = false;
                SetAlarmButton.IsEnabled = true;
            }
        }

        private void CountdownThreadFunction()
        {
            try
            {
                // Implement the countdown logic here
                while (remainingTime.TotalSeconds > 0 && !isCountdownStopped)
                {
                    if (!isCountdownPaused)
                    {
                        // Update the UI with the current remaining time
                        Dispatcher.Invoke(() => UpdateTimer(null, null));

                        // Wait for 1 second before the next update
                        Thread.Sleep(1000);

                        // Decrement the remaining time
                        lock (lockObject)
                        {
                            remainingTime = remainingTime.Subtract(TimeSpan.FromSeconds(1));
                        }
                    }
                    else
                    {
                        // Wait until the countdown is resumed
                        Thread.Sleep(100);
                    }
                }

                // Countdown is done or stopped, display a message
                Dispatcher.Invoke(() =>
                {
                    if (isCountdownStopped)
                        CountdownLabel.Content = "Countdown Stopped";
                    else
                        CountdownLabel.Content = "Countdown Finished!";
                    StartButton.IsEnabled = true;
                    PauseResumeButton.IsEnabled = false;
                    ResetButton.IsEnabled = true;
                    SetAlarmButton.IsEnabled = true;
                });
            }
            catch (ThreadInterruptedException)
            {
                // Handle thread interruption if needed
            }
        }

        private void PauseResumeButton_Click(object sender, RoutedEventArgs e)
        {
            // Implement the logic to pause or resume the countdown timer
            if (isCountdownPaused)
            {
                // Resume the countdown
                isCountdownPaused = false;
                PauseResumeButton.Content = "Pause";
            }
            else
            {
                // Pause the countdown
                isCountdownPaused = true;
                PauseResumeButton.Content = "Resume";
            }
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            // Implement the logic to reset the countdown timer
            isCountdownPaused = false;
            isCountdownStopped = true;
            remainingTime = originalTime;
            CountdownLabel.Content = originalTime.ToString(@"hh\:mm\:ss");
            StartButton.IsEnabled = true;
            PauseResumeButton.IsEnabled = false;
            ResetButton.IsEnabled = false;
        }

        private void SetAlarmButton_Click(object sender, RoutedEventArgs e)
        {
            // Open the AlarmSetupWindow to set the alarm time
            AlarmWindow alarmSetupWindow = new AlarmWindow();
            alarmSetupWindow.ShowDialog();

            // Check if the alarm was set and get the alarm time from the AlarmSetupWindow
            if (alarmSetupWindow.IsAlarmSet)
            {
                alarmTime = alarmSetupWindow.AlarmTime;
                isAlarmSet = true;

                // Start the timer to check for the alarm
                TimeSpan timeUntilAlarm = alarmTime - DateTime.Now;
                if (timeUntilAlarm.TotalSeconds < 0)
                {
                    // If the alarm time has already passed today, set it for tomorrow
                    timeUntilAlarm = timeUntilAlarm.Add(TimeSpan.FromDays(1));
                }

                timer.Interval = timeUntilAlarm;
                timer.Start();

                MessageBox.Show($"Alarm set for {alarmTime:hh:mm tt}.");
            }
        }
    }
}
